# nginx-rtmp in Docker
Stream to multiple platforms like Twitch and YouTube at the same time.
## Architecture
![nginx-rtmp architecture](./nginx-rtmp.drawio.png)
## Prereqs
1. Docker

I'm running this on a Raspberry Pi, but there are several other options:
* The same computer you stream from
  * Docker install instructions for [Windows](https://docs.docker.com/desktop/install/windows-install/)
* An old PC in a closet somewhere in your house (imaging it with Linux would be easiest)
* A smol remote cloud server on something like [Digital Ocean](https://m.do.co/c/0e35bfae718b) (and [this tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04))

On whatever option you choose, [install Docker](https://docs.docker.com/engine/install/).

2. Raspberry Pi (optional)

Raspberry Pis are neat little servers, but currently way overpriced due to the global supply chain woes. If you have a Raspberry Pi or procure one, just [follow their guide](https://www.raspberrypi.com/documentation/computers/getting-started.html). I use Debian Lite, which means no GUI, but it won't matter if you want that.

3. Streaming keys from Twitch and YouTube

4. Get these files onto your computer somehow, like [with git](https://www.w3schools.com/git/default.asp)
## How to use
Paste your streaming keys by replacing `your-stream-key` in the following lines of `nginx.conf`:
```
#stream to twitch
push rtmp://live.twitch.tv/app/your-stream-key;
#stream to youtube 
push rtmp://a.rtmp.youtube.com/live2/your-stream-key;
``` 
You'll run these commands from the terminal, whether on Linux or on Windows.

Build image:

(make sure you're in this directory in the terminal! Use `cd` to get there, e.g. `cd ~/.code/surfingdoggo/tutorials/nginx-rtmp` or wherever you store this)
```
docker build -t nginx-rtmp . --progress=plain
```
Run image:
```
docker run --rm -d -p 1935:1935 --name nginx-rtmp nginx-rtmp:latest
```
Debug errors:
```
docker logs nginx-rtmp
```
## How to test with OBS Studio and VLC
* Run a container with the command above

* Open [OBS Studio](https://obsproject.com/)
* Click the "Settings" button
* Go to the "Stream" section
* In "Stream Type" select "Custom Streaming Server"
* In the "URL" enter the `rtmp://<ip_of_host>/live` replacing `<ip_of_host>` with the IP of the host in which the container is running, or if local (e.g. Docker is running on the same computer you stream from), either `localhost` or `127.0.0.1`. For example: `rtmp://192.168.0.30/live` or `rtmp://localhost/live`
* In the "Stream key" use a "key" that will be used later in the client URL to display that specific stream. For example: `stream`
* Click the "OK" button
* In the section "Sources" click the "Add" button (`+`) and select a source (for example "Screen Capture") and configure it as you need
* Click the "Start Streaming" button

* Open a [VLC](http://www.videolan.org/vlc/index.html) player
* Click "Open Network Stream"
* Enter the URL from above as `rtmp://<ip_of_host>/live/<key>` replacing `<ip_of_host>` with the IP of the host in which the container is running and `<key>` with the key you created in OBS Studio. For example: `rtmp://192.168.0.30/live/stream`
* Click "Play"
* Now VLC should start playing whatever you are transmitting from OBS Studio

## Sources
Here's the [nginx-rtmp documentation](https://github.com/arut/nginx-rtmp-module/wiki/Directives).

Credit and much thanks to [Tiangolo](https://github.com/tiangolo/nginx-rtmp-docker) for the nginx-rtmp Dockerfile!

Thanks to @vuurvoske for [this guide](https://www.vuurvoske426.dev/post/2022-01-25-setting-up-obs-for-twitch-with-nginx-rtmp-relay/) on nginx-rtmp configuration!
